# stocks-service

A RESTful web service for an online shop project to provide :
- Update product stocks.
-  Query product stocks.
-  Provide statistics about product stocks.