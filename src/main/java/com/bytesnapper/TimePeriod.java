package com.bytesnapper;

public enum TimePeriod {

    TODAY("today"),
    LAST_MONTH("lastMonths");

    private String timePeriod;

    TimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
