package com.bytesnapper.controller;

import com.bytesnapper.TimePeriod;
import com.bytesnapper.model.Stock;
import com.bytesnapper.repository.StockRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StocksController {


    private final StockRepository stockRepository;


    public StocksController(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @PostMapping("/updateStock")
    ResponseEntity updateStock(@RequestBody Stock stock) {
        ResponseEntity responseEntity = null;
        if (stockRepository.update(stock)) {
            responseEntity = new ResponseEntity(HttpStatus.CREATED);
        } else {
            responseEntity = new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return responseEntity;
    }

    @GetMapping("/stock")
    Stock getStock(@RequestParam(value = "productId") String productId) {
        return null;
    }

    @GetMapping("/Statistics")
    Stock getStatistics(@RequestParam(value = "time") TimePeriod timePeriod) {
        return null;
    }
}
