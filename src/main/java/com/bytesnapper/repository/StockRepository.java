package com.bytesnapper.repository;

import com.bytesnapper.model.Stock;

import java.util.HashMap;
import java.util.Map;

/**
 * A @{@link StockRepository is simulates for database storage
 */
public class StockRepository {

    private Map<String, Stock> stocks = new HashMap<>();

    public boolean update(Stock stock) {
        stocks.get(stock.getId()).setProductId(stock.getProductId());
        stocks.get(stock.getId()).setQuantity(stock.getQuantity());
        stocks.get(stock.getId()).setTimeStamp(stock.getTimeStamp());
        //TODO return false is someting went wrong
        return true;
    }


    public Stock getStock(String productId) {
        //TODO return matching stock
        return null;
    }
}
